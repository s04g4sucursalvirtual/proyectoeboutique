-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Servidor: sql3.freesqldatabase.com
-- Tiempo de generación: 09-10-2021 a las 00:29:54
-- Versión del servidor: 5.5.54-0ubuntu0.12.04.1
-- Versión de PHP: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sql3442888`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproductoPK` int(11) NOT NULL,
  `procategoria` varchar(45) NOT NULL,
  `pronombre` varchar(45) NOT NULL,
  `prodescripcion` varchar(45) NOT NULL,
  `promarca` varchar(45) NOT NULL,
  `protalla` varchar(45) NOT NULL,
  `procolor` varchar(45) NOT NULL,
  `proimagen` varchar(150) NOT NULL,
  `proprecio` int(11) NOT NULL,
  `prostock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproductoPK`, `procategoria`, `pronombre`, `prodescripcion`, `promarca`, `protalla`, `procolor`, `proimagen`, `proprecio`, `prostock`) VALUES
(1, 'Vestido', 'Vestido estampado con fajon', 'Vestido midi manga tres cuartos. Con estampad', 'Esprit', 'M', 'Gris Estampado', 'https://esprit.vteximg.com.br/arquivos/ids/820518-1112-1370/34_493B700_091300_0.jpg?v=637662768504470000', 259900, 6),
(2, 'Chaqueta', 'Chaqueta a rayas', 'Chaqueta tejida, cuello redondo. Diseño a ray', 'Esprit', 'S', 'Gris con rayas', 'https://esprit.vteximg.com.br/arquivos/ids/776227-1112-1370/34_621B205_090000_0.jpg?v=637590288969630000', 94950, 8),
(3, 'Buzo', 'Suéter biodegradable con mezcla de lino', 'Suéter con cuello en V y manga larga. Esta pr', 'Esprit', 'M', 'Verde', 'https://esprit.vteximg.com.br/arquivos/ids/821453-1112-1370/34_662C700_100000_0.jpg?v=637665564489870000', 198900, 4),
(5, 'Falda', 'Falda corta', 'Falda corta en denim, tiro alto. Acabado, con', 'Denim', 'L', 'Negro', 'https://esprit.vteximg.com.br/arquivos/ids/671414-1112-1370/34_949A002_090000_0.jpg?v=637504524115100000', 70, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproductoPK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
