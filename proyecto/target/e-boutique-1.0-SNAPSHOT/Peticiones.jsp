<%-- 
    Document   : Archivo de peticiones
    Created on : dd/mm/yyyy, hh:mm: AM/PM
    Author     : nombre autor

--%>


<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="logica.Producto"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="application/json;charset=iso-8859-1" language="java" pageEncoding="iso-8859-1" session="true"%>

<%    // Iniciando respuesta JSON.
    String respuesta = "{";

    //Lista de procesos o tareas a realizar 
    List<String> tareas = Arrays.asList(new String[]{
        "actualizarcontacto",
        "eliminarcontacto",
        "listarcontacto",
        "guardarContacto"
    });
    
    String proceso = "" + request.getParameter("proceso");
   

    // Validaci�n de par�metros utilizados en todos los procesos.
    if (tareas.contains(proceso)) {
        respuesta += "\"ok\": true,";
        // ------------------------------------------------------------------------------------- //
        // -----------------------------------INICIO PROCESOS----------------------------------- //
        // ------------------------------------------------------------------------------------- //
        if (proceso.equals("guardarProducto")) {
            
            
            
            
            
        int idproductoPK = Integer.parseInt(request.getParameter("idproductoPK"));
     String procategoria = request.getParameter("procategoria"); 
     String pronombre  = request.getParameter("pronombre ");
     String prodescripcion  = request.getParameter("prodescripcion");
     String promarca = request.getParameter("promarca"); 
     String protalla = request.getParameter("protalla"); 
     String procolor = request.getParameter("procolor"); 
     String proimagen = request.getParameter("proimagen"); 
        int proprecio = Integer.parseInt(request.getParameter("proprecio")); 
        int prostock  = Integer.parseInt(request.getParameter("prostock"));
            boolean favorito = Boolean.parseBoolean(request.getParameter("favorito"));
      
            Producto p = new Producto();
            p.setIdproductoPK(idproductoPK);
            p.setProcategoria(procategoria);
            p.setPronombre(pronombre);
            p.setProdescripcion(prodescripcion);
            p.setPromarca(promarca);
            p.setProtalla(protalla);
            p.setProcolor(procolor);
            p.setProimagen(proimagen);
            p.setProprecio(proprecio);
            p.setProstock(prostock);
            
            
            if (p.guardarProducto()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("eliminarproducto")) {
            int idproductoPK = Integer.parseInt(request.getParameter("idproductoPK"));
            //su codigo ac�
            Producto p = new Producto();
             p.setIdproductoPK(idproductoPK);
            
            
            
            if (p.borrarProducto(idproductoPK)) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }

        } else if (proceso.equals("listarproducto")) {
            //su codigo ac�
               Producto p = new Producto();
            try {
                List<Producto> lista = p.listarProducto();
                respuesta += "\"" + proceso + "\": true,\"Producto\":" + new Gson().toJson(lista);
            } catch (SQLException ex) {
                respuesta += "\"" + proceso + "\": true,\"Producto\":[]";
                Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (proceso.equals("actualizarcontacto")) {
            
             int idproductoPK = Integer.parseInt(request.getParameter("idproductoPK"));
     String procategoria = request.getParameter("procategoria"); 
     String pronombre  = request.getParameter("pronombre ");
     String prodescripcion  = request.getParameter("prodescripcion");
     String promarca = request.getParameter("promarca"); 
     String protalla = request.getParameter("protalla"); 
     String procolor = request.getParameter("procolor"); 
     String proimagen = request.getParameter("proimagen"); 
        int proprecio = Integer.parseInt(request.getParameter("proprecio")); 
        int prostock  = Integer.parseInt(request.getParameter("prostock"));
            boolean favorito = Boolean.parseBoolean(request.getParameter("favorito"));
            //su codigo ac�
            Producto p = new Producto();
            p.setIdproductoPK(idproductoPK);
            p.setProcategoria(procategoria);
            p.setPronombre(pronombre);
            p.setProdescripcion(prodescripcion);
            p.setPromarca(promarca);
            p.setProtalla(protalla);
            p.setProcolor(procolor);
            p.setProimagen(proimagen);
            p.setProprecio(proprecio);
            p.setProstock(prostock);
           
            
            if (p.actualizarProducto()) {
                respuesta += "\"" + proceso + "\": true";
            } else {
                respuesta += "\"" + proceso + "\": false";
            }
        }

        // ------------------------------------------------------------------------------------- //
        // -----------------------------------FIN PROCESOS-------------------------------------- //
        // ------------------------------------------------------------------------------------- //
        // Proceso desconocido.
    } else {
        respuesta += "\"ok\": false,";
        respuesta += "\"error\": \"INVALID\",";
        respuesta += "\"errorMsg\": \"Lo sentimos, los datos que ha enviado,"
                + " son inv�lidos. Corrijalos y vuelva a intentar por favor.\"";
    }
    // Usuario sin sesi�n.
    // Responder como objeto JSON codificaci�n ISO 8859-1.
    respuesta += "}";
    response.setContentType("application/json;charset=iso-8859-1");
    out.print(respuesta);
%>
