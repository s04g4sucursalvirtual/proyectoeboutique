/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException; 
import java.util.logging.Level; 
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;


public class Producto {
  // atributos
  private int idproductoPK;  
  private String procategoria;
  private String pronombre;
  private String prodescripcion;
  private String promarca;
  private String protalla;
  private String procolor;
  private String proimagen;
  private int proprecio;
  private int prostock;
// constructores

    public Producto() {
    }

    public Producto(int idproductoPK, String procategoria, String pronombre, String prodescripcion, String promarca, String protalla, String procolor, String proimagen, int proprecio) {
        this.idproductoPK = idproductoPK;
        this.procategoria = procategoria;
        this.pronombre = pronombre;
        this.prodescripcion = prodescripcion;
        this.promarca = promarca;
        this.protalla = protalla;
        this.procolor = procolor;
        this.proimagen = proimagen;
        this.proprecio = proprecio;
        this.prostock = prostock;
    }
  // encapsular

    public int getIdproductoPK() {
        return idproductoPK;
    }

    public void setIdproductoPK(int idproductoPK) {
        this.idproductoPK = idproductoPK;
    }

    public String getProcategoria() {
        return procategoria;
    }

    public void setProcategoria(String procategoria) {
        this.procategoria = procategoria;
    }

    public String getPronombre() {
        return pronombre;
    }

    public void setPronombre(String pronombre) {
        this.pronombre = pronombre;
    }

    public String getProdescripcion() {
        return prodescripcion;
    }

    public void setProdescripcion(String prodescripcion) {
        this.prodescripcion = prodescripcion;
    }

    public String getPromarca() {
        return promarca;
    }

    public void setPromarca(String promarca) {
        this.promarca = promarca;
    }

    public String getProtalla() {
        return protalla;
    }

    public void setProtalla(String protalla) {
        this.protalla = protalla;
    }

    public String getProcolor() {
        return procolor;
    }

    public void setProcolor(String procolor) {
        this.procolor = procolor;
    }

    public String getProimagen() {
        return proimagen;
    }

    public void setProimagen(String proimagen) {
        this.proimagen = proimagen;
    }

    public int getProprecio() {
        return proprecio;
    }

    public void setProprecio(int proprecio) {
        this.proprecio = proprecio;
    }

    public int getProstock() {
        return prostock;
    }

    public void setProstock(int prostock) {
        this.prostock = prostock;
    }
 public boolean guardarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO producto(procategoria,pronombre,prodescripcion,protalla,procolor,proimagen,proprecio,prostock) "
                + " VALUES ( '" + this.procategoria + "','" + this.pronombre + "',"
                + "'" + this.prodescripcion + "','" + this.promarca + "','" + this.protalla + "',"
                + "'" + this.procolor + "','" + this.proimagen + "','" +  this.proprecio +  "','" + this.prostock+ "');  ";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean borrarProducto(int idproductoPK) {
        String Sentencia = "DELETE FROM `contactos` WHERE `idproductoPK`='" + idproductoPK+ "'";
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarProducto() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE `producto` SET procategoria='" + this.procategoria + "',pronombre='" +  this.pronombre + "',prodescripcion='" + this.prodescripcion
                + "',promarca='" + this.promarca + "',protalla='" + this.protalla+ "',procolor='" + this.procolor + "',proimagen='" +  this.proimagen + "',proprecio='" +  this.proprecio + "',prostock='" + this.prostock
                +  "' WHERE idproductoPK=" + this.idproductoPK + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public List<Producto> listarProducto() throws SQLException {
        ConexionBD conexion = new ConexionBD();
        List<Producto> listaProductos = new ArrayList<>();
        String sql = "select * from producto;";
        ResultSet rs = conexion.consultarBD(sql);
        Producto p;
        while (rs.next()) {
            p = new Producto();
            p.setIdproductoPK(rs.getInt("idproductoPK"));
            p.setProcategoria(rs.getString("procategoria"));
            p.setPronombre(rs.getString("pronombre"));
            p.setProdescripcion(rs.getString("prodescripcion"));
            p.setPromarca(rs.getString("promarca"));
            p.setProtalla(rs.getString("protalla"));
            p.setProcolor(rs.getString("procolor"));
            p.setProimagen(rs.getString("proimagen"));
            p.setProprecio(rs.getInt("proprecio"));
            p.setProstock(rs.getInt("prostock"));
            listaProductos.add(p);

        }
        conexion.cerrarConexion();
        return listaProductos;
    }

    public Producto getProducto() throws SQLException {
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from Producto where idproductoPK='" + this.idproductoPK + "'";
        ResultSet rs = conexion.consultarBD(sql);
        if (rs.next()) {         
            this.idproductoPK = rs.getInt("idproductoPK");
            this.procategoria = rs.getString("procategoria");
            this.pronombre = rs.getString("pronombre");
            this.prodescripcion = rs.getString("prodescripcion");
            this.promarca = rs.getString("promarca");
            this.protalla = rs.getString("protalla");
            this.procolor = rs.getString("procolor");
            this.proimagen = rs.getString("proimagen");
            this.proprecio = rs.getInt("proprecio");
            this. prostock = rs.getInt(" prostock");
            conexion.cerrarConexion();
            return this;

        } else {
            conexion.cerrarConexion();
            return null;
        }

    }

    @Override
    public String toString() {
        return "Producto{" + "idproductoPK=" + idproductoPK + ", procategoria=" + procategoria + ", pronombre=" + pronombre + ", prodescripcion=" + prodescripcion + ", promarca=" + promarca + ", protalla=" + protalla + ", procolor=" + procolor + ", proimagen=" + proimagen + ", proprecio=" + proprecio + ", prostock=" + prostock + '}';
    }
}
  