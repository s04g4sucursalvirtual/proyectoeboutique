git config --global user.name "Alejandro León"
git config --global user.email "juanalejandrol15@gmail.com"

Crear un nuevo repositorio
git clone https://gitlab.com/s04g4sucursalvirtual/proyectoeboutique.git
cd proyectoeboutique
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push a una carpeta existente
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/s04g4sucursalvirtual/proyectoeboutique.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push a un repositorio de Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/s04g4sucursalvirtual/proyectoeboutique.git
git push -u origin --all
git push -u origin --tags